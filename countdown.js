
  function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    };
  }

  function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var breakDiv = document.getElementById('break');
    var tree = document.getElementById('scene');
    var banner = document.getElementById('banner');
    // var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
      var t = getTimeRemaining(endtime);

      // daysSpan.innerHTML = t.days;
      hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
      minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
      secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

      if (t.total <= 0) {
        clearInterval(timeinterval);
        clearInterval(imageInterval);
        document.body.style.background = '#000000';
        clock.style.display= 'none';
        breakDiv.style.display= 'none';
        tree.style.display= 'block';
        logoImage.style.display='none';
        banner.style.display='none';
        displayTree();
      }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
  }

  var logoImage = document.getElementById('logoImage');
  var src = ["logos/aplLib.jpg","logos/anthony1.jpg",'logos/BergstromAutomotive_4c.png',
    'logos/Boldt_202_4c.png','logos/cake_guru.jpg','logos/candeo_vendors.png',
    'logos/CF_Fox_Valley_logo_JPG.jpg','logos/CFCULogo4c.jpg','logos/city_of_appleton.png',
    'logos/crazy_sweet.jpg','logos/Downtown_Appleton.png','logos/FCC_Octoberfest_35th.jpg',
    'logos/fox_cities_kiwanis.jpg','logos/Fox_Valley_Memory_project.jpg',
    'logos/History-Museum.jpg','logos/Katapult_logo.jpg',
    'logos/le_prince_crepes.jpg','logos/Looy_Dogs.jpg',
    'logos/makaroff_youth_ballet.jpg','logos/Marcia_School_of_Dance.jpg',
    'logos/mikey_air.jpg','logos/Minus_One_Quartet.jpg',
    'logos/Muncheez.jpg',
    'logos/PAC.jpg','logos/PC_mastwtag.jpg','logos/PDC.jpg','logos/Rock_School.jpg',
    'logos/roller_derby.png','logos/RSPR.png',
    'logos/runaway_shoes.jpg','logos/savage_henry.png','logos/Skyline_Logo.jpg',
    'logos/strathmore_logo_rgb.png','logos/timber_rattlers.jpg',
    'logos/Tundraland.png','logos/uwgb.jpg',
    'logos/Vento_logo.jpg','logos/wbay.jpg','logos/wbay.jpg','logos/WPRlogo_color.png','logos/wpt_rounded.png'
  ];
  var step = 0;
  var maxStep = src.length;

  function rotateImages() {
       logoImage.src = src[step];
       logoImage.alt = src[step];
       if(step<maxStep-1){
           step++;
       }else{
           step=0;
       }
   }
   var imageInterval = setInterval(rotateImages,3000);
